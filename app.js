async function run(flags){
    const express = require('express');

    const app = express();

    process.env.FLAGS = await flags;

    const exphbs = require('express-handlebars');
    app.engine('handlebars', exphbs({ defaultLayout: 'main' }));
    app.set('view engine', 'handlebars');

    const indexRouter = require('./routes/index');
    const babe = require('./routes/babe');

    app.use(express.json());
    app.use(express.urlencoded({ extended: false }));

    app.use('/', indexRouter);
    app.use('/babe', babe);

    const PORT = process.env.PORT || 3000;
    // console.log(process.env.DEVDBPASS);
    app.listen(PORT, () => console.log(`Listening on port ${PORT}!`));
}
run(process.argv[2]);

process.on('unhandledRejection', error => {
    console.error(error);
    process.exit();
});