CREATE TABLE users(
    id bigserial PRIMARY KEY,
    displayName TEXT,
    dateRegistered DATE NOT NULL,
    discordAuth TEXT? NOT NULL
);

CREATE TABLE list(
    id bigserial PRIMARY KEY,
    itemName TEXT NOT NULL,
    dateAdded DATE NOT NULL,
    dateUpdated DATE NOT NULL,
    progress TEXT NOT NULL,
    category TEXT NOT NULL,
    imageSketch TEXT,
    sketchDateAdded DATE,
    sketchDateUpdate DATE,
    imageLineart TEXT,
    lineartDateAdded DATE,
    lineartDateUpdate DATE,
    imageFlats TEXT,
    flatsDateAdded DATE,
    flatsDateUpdate DATE,
    imageShading TEXT,
    shadingDateAdded DATE,
    shadingDateUpdate DATE,
    imageBG TEXT,
    bgDateAdded DATE,
    bgDateUpdate DATE,
    imageFinal TEXT,
    finalDateAdded DATE,
    finalDateUpdate DATE
);